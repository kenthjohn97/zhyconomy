package com.kenthjohn.minecraft;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

public final class Zhyconomy extends JavaPlugin {

    private ZhyconomyEcon zhyconomy;

    @Override
    public void onEnable() {
        // Plugin startup logic
        getLogger().info("Enabling Zhyconomy.");

        setupEconomy();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        getLogger().info("Shutting down Zhyconomy.");
    }

    private void setupEconomy() {

        zhyconomy = new ZhyconomyEcon();

        getServer().getServicesManager().register(Economy.class, zhyconomy, this, ServicePriority.Highest);

    }
}
